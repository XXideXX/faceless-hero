using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(abstractdungeonGEN), true)]


public class RandomDungeonGENEdior : Editor
{

    abstractdungeonGEN abstractdungeonGEN;

    private void Awake()
    {
        abstractdungeonGEN = (abstractdungeonGEN)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if(GUILayout.Button("Create dungeon"))
        {
            abstractdungeonGEN.GenerateDungeaon() ;

        }
    }
}
