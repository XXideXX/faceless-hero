﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(abstractdungeonGEN), true)]
public class RandomDungeonGeneratorEditor : Editor
{
    abstractdungeonGEN generator;

    private void Awake()
    {
        generator = (abstractdungeonGEN)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if(GUILayout.Button("Create Dungeon"))
        {
            generator.GenerateDungeaon();
        }
    }
}
