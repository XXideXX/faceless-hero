
using UnityEngine;

[CreateAssetMenu(menuName = "Assets/InventarMaerialovivsyakoydresni/Nerazrusiaemievesi")]
public class ScriptNerasrusaemgoobecta : ScriptableObject
{

    public int count;
    public Sprite sprite;
    public string name;
    public string type;
}