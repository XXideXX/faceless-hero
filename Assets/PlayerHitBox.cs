using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHitBox : MonoBehaviour
{
    Player piderast;
    private float nexfiretime;
    private void Start()
    {
        piderast = GetComponentInParent<Player>();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
       
        if (collision.gameObject.tag == "Fighter" && collision.gameObject.name != "Player" && nexfiretime < Time.time)
        {
      
            

            nexfiretime = Time.time + piderast.hitrate;
            collision.GetComponent<Fighter>().RecievDMG(piderast.damage, null);
        }

    }
}