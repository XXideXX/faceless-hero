using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PizdatiyHuy : Mover
{
    private Transform playertransform;
    private BoxCollider2D hitbox;
    [SerializeField] private float lineOfSite;
    [SerializeField] private float shootingrange;

    public int damage;
    [SerializeField] public float firerate = 1;
    private float nexfiretime;
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform bulletparent;

    protected override void Start()
    {
        base.Start();
        playertransform = GameMananger.instanse.player.transform;
        var player = FindObjectOfType<Player>();
        hitbox = transform.GetChild(0).GetComponent<BoxCollider2D>();
        Physics2D.IgnoreCollision(player.GetComponent<BoxCollider2D>(), GetComponent<BoxCollider2D>());
        skorost += UnityEngine.Random.Range(-0.4f, 0.4f);
    }

    private void FixedUpdate()
    {

        float distancefromplayer = Vector2.Distance(playertransform.position, transform.position);
        Vector3 dir = playertransform.position - transform.position;
        if (distancefromplayer < lineOfSite && distancefromplayer > shootingrange)
        {

        }
        else if (distancefromplayer <= shootingrange && nexfiretime < Time.time)
        {
            animator.SetFloat("attMultiplier", 1f);
            animator.SetTrigger("�����");

            GameObject buleetOBJ = Instantiate(bullet, bulletparent.transform.position, Quaternion.identity);
       
            buleetOBJ.GetComponent<enemyBullet>().bulletDMG = damage;

            Physics2D.IgnoreLayerCollision(9, 3);
            nexfiretime = Time.time + firerate;
            // animator.SetBool("�����", false);
        }

        movegeroy(dir);

    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lineOfSite);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, shootingrange);
    }
}
