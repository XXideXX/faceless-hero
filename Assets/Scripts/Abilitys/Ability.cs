using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : ScriptableObject
{
    public Sprite icon;
    public bool active;
    public new string name;
    public float cooldowntime;
    public float activetime;
    [TextArea(1,3)] public string opisanie;
    

    public virtual void Activate(GameObject parent,Vector2 direction)   { }
    public virtual void BeginCooldown(GameObject parent, Vector2 direction) { }
}
