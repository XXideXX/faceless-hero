using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityButton : MonoBehaviour
{
    public Ability ability;
    private Player piderast;
   [SerializeField] private bool aimed = false;
    float activetime;
    float cooldowntime;
    FixedJoystick fixedJoystick;
    [SerializeField] GameObject huyumbula;
    [SerializeField]private Vector2 direction;
    private void Start()
    {
        piderast = FindObjectOfType<Player>();
        fixedJoystick = GetComponent<FixedJoystick>();
    }
    enum AbilityState
    {
        ready,
        active,
        cooldown

    }
    AbilityState state = AbilityState.ready;

    public void OnButtonClick()
    {
        if (ability != null)
        {
            switch (state)
            {
                case AbilityState.ready:
                   ability.Activate(piderast.gameObject , direction);
                   state = AbilityState.active;
                   activetime = ability.activetime;
                break;
            }
        }

     /*  
         if (ability != null)
        {
            piderast.ability = ability;
            piderast.button = button;
        }
    */
    }
    private void Update()
    {
        
       /* if(fixedJoystick.Direction.x != 0 | fixedJoystick.Direction.y != 0)
        {
            direction = fixedJoystick.Direction;
        }
       */
        if (fixedJoystick.DeadZone < fixedJoystick.Direction.magnitude)
        {
            aimed = true;
            huyumbula.SetActive(true);
            var angle = Mathf.Atan2(fixedJoystick.Vertical, fixedJoystick.Horizontal);
            
            huyumbula.gameObject.transform.rotation = Quaternion.Euler(0, 0, (angle * 60) - 90);
            var x = Mathf.Cos(angle); //* radius
            var y = Mathf.Sin(angle);

            huyumbula.transform.position = new Vector2(x + piderast.transform.position.x, y + piderast.transform.position.y);
        }
        else
        {
        
            huyumbula.SetActive(false);
            aimed = false;
        }

        direction = fixedJoystick.Direction;

        if (ability != null)
        {
            switch (state)
            {
                case AbilityState.active:
                    if (activetime > 0)
                    {
                        activetime -= Time.deltaTime;
                    }
                    else
                    {
                        ability.BeginCooldown(piderast.gameObject , fixedJoystick.Direction);
                        state = AbilityState.cooldown;
                        cooldowntime = ability.cooldowntime;
                    }
                    break;
                case AbilityState.cooldown:
                    if (cooldowntime > 0)
                    {
                        cooldowntime -= Time.deltaTime;
                    }
                    else
                    {
                        state = AbilityState.ready;

                    }
                    break;
            }
        }
       
    }
}
