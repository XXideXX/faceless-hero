using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AbilityHolder : MonoBehaviour
{
    public Ability ability;
    float cooldowntime;
    float activetime;
    public  Button button;
    enum AbilityState
    {
        ready,
        active,
        cooldown
    }
    AbilityState state = AbilityState.ready;

  
        
      

     void Update()
    {
        if(ability != null)
        {
        switch(state)
        {
            case AbilityState.ready:
                if (button.enabled == true)
                {
                    ability.Activate(gameObject, new Vector2(0,0));
                    state = AbilityState.active;
                    activetime = ability.activetime; 
                }
                break;
            case AbilityState.active:
                if(activetime > 0)
                {
                    activetime -= Time.deltaTime;
                }
                else
                {
                    ability.BeginCooldown(gameObject ,new Vector2(0,0));
                    state = AbilityState.cooldown;
                    cooldowntime = ability.cooldowntime;
                }
                break;
            case AbilityState.cooldown:
                if (activetime > 0)
                {
                    activetime -= Time.deltaTime;
                }
                else
                {
                    state = AbilityState.ready;

                }
                break;


            }
        }

    }
   
}
