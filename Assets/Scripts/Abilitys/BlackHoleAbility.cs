using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class BlackHoleAbility : Ability
{
    [SerializeField] private GameObject fireblast;
    [HideInInspector] public bool IgnighrDebuffenabeled;
    [SerializeField] private float ManaCost;


    public override void Activate(GameObject parent, Vector2 direction)
    {
        if (parent.GetComponent<Player>().ManaCapasity >= ManaCost)
        {
            parent.GetComponent<Player>().ManaCapasity -= ManaCost;
            var buleetOBJ = Instantiate(fireblast, parent.transform.position, Quaternion.identity);

            buleetOBJ.GetComponent<BlackholeGO>().damage = parent.GetComponent<Player>().damage;
            buleetOBJ.GetComponent<BlackholeGO>().heromana = parent.GetComponent<Player>().ManaCapasity;

            buleetOBJ.GetComponent<BlackholeGO>().debufficon = icon;
        }
    }
    public override void BeginCooldown(GameObject parent, Vector2 direction)
    {
    }
}
