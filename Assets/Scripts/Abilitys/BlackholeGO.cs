using Assets.abilitys;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackholeGO : MonoBehaviour
{

    [HideInInspector] public int damage;
    [HideInInspector] public Sprite debufficon;
    [HideInInspector] public float heromana;
    ArcaneDebuff debuff;
    private float lasttick;
    private float tickdmgcd = 1;
    private void Start()
    {       
        debuff = new ArcaneDebuff(debufficon);
        debuff.MyTickDamage = (int)heromana;
    }

   

    private void OnTriggerEnter2D(Collider2D coll) // ����� ����������
    {
       
        
        if (coll.gameObject.name != "Player")
        {
            if (coll.gameObject.tag == "Fighter")
            {
           //   if(lasttick < Time.time)
           //   { 

                   Debuff clone = debuff.Clone();
                   clone.Apply(coll.gameObject.GetComponent<Fighter>());
                   coll.gameObject.GetComponent<Fighter>().RecievDMG(damage, debuff);
                   lasttick = tickdmgcd + Time.time;
            //  }
            }
        }
    }



}
