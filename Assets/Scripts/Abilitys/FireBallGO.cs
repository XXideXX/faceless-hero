using Assets.abilitys;
using UnityEngine;

public class FireBallGO : MonoBehaviour
{
     [HideInInspector] public int damage;
     [HideInInspector] public Sprite debufficon;
     [HideInInspector] public bool IgnightTalant;
     IgniteDebuff debuff;
   
    private void Start()
    {
        if(IgnightTalant)
        { 
           debuff = new IgniteDebuff(debufficon);
        }
    }

    private  void OnCollisionEnter2D(Collision2D coll)  
    {
        
            if (coll.gameObject.name != "Player" )
            {

                if (coll.gameObject.tag == "Fighter")
                {
                    if (debuff != null)
                 {
                    Debuff clone = debuff.Clone();
                    clone.Apply(coll.gameObject.GetComponent<Fighter>());
                 }

                   coll.gameObject.GetComponent<Fighter>().RecievDMG(damage , debuff) ;

                }

                  Destroy(gameObject);

            }
        
      
    }
   
}

