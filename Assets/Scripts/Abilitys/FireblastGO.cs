using Assets.abilitys;
using UnityEngine;

public class FireblastGO : MonoBehaviour
{

    [HideInInspector] public int damage;
    [HideInInspector] public Sprite debufficon;

    IgniteDebuff debuff;
    private void Start()
    {
        if ( SkillMananger.instance.IgnightTalant)
        {
            debuff = new IgniteDebuff(debufficon);
        }
    }
    

    private void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.gameObject.name != "Player")
        {
            if (coll.gameObject.tag == "Fighter")
            {
                  if (debuff != null)
                  {
                        Debuff clone = debuff.Clone();
                        clone.Apply(coll.gameObject.GetComponent<Fighter>());
                  }
                   coll.gameObject.GetComponent<Fighter>().RecievDMG(damage, debuff);
            }
        }
    }
}


