using UnityEngine;


[CreateAssetMenu]
public class FireblastSpell : Ability
{
    [SerializeField] private GameObject fireblast;
   [HideInInspector] public bool IgnighrDebuffenabeled;
    [SerializeField] private float ManaCost;


    public override void Activate(GameObject parent, Vector2 direction)
    {
        if (parent.GetComponent<Player>().ManaCapasity >= ManaCost)
        {
            parent.GetComponent<Player>().ManaCapasity -= ManaCost;
            var buleetOBJ = Instantiate(fireblast, parent.transform.position, Quaternion.identity);

          buleetOBJ.GetComponent<FireblastGO>().damage = parent.GetComponent<Player>().damage;
  
            buleetOBJ.GetComponent<FireblastGO>().debufficon = icon;
        }
    }
    public override void BeginCooldown(GameObject parent, Vector2 direction)
    {
    }
}
