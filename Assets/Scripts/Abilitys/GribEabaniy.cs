using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GribEabaniy : Ability
{
    [SerializeField] private GameObject gribobject;
    [SerializeField]List<GameObject> gribistalantom = new List<GameObject>();
    [SerializeField] private float ManaCost;

    public override void Activate(GameObject parent, Vector2 direction)
    {
        if (parent.GetComponent<Player>().ManaCapasity >= ManaCost)
        {
            parent.GetComponent<Player>().ManaCapasity -= ManaCost;
            GameObject buleetOBJ = Instantiate(gribobject, parent.transform.position, Quaternion.identity);
    
            buleetOBJ.GetComponent<GribEbaniyGO>().damage = parent.GetComponent<Player>().damage;
            buleetOBJ.GetComponent<GribEbaniyGO>().debufficon = icon;
             gribistalantom.Add(buleetOBJ);
           if (gribistalantom.Count > 1) // ��� ������ �� ������ ����  ����� � �� ����� ������� ������ ���� 
           {
            
              Destroy(gribistalantom[0]);
              gribistalantom.Remove(gribistalantom[0]);

           }
        }

    }
    public override void BeginCooldown(GameObject parent, Vector2 direction)
    {
    }
}
