using Assets.abilitys;
using UnityEngine;

public class GribEbaniyGO : MonoBehaviour
{
    [SerializeField] private float CD;
    private Animator animator ;
    private float nexfiretime;
    public Sprite debufficon;
    [SerializeField] private GameObject explosion;
    [HideInInspector] public int damage;
    PoisonDebuff debuff;
   
    private void Start()
    {
        animator = GetComponent<Animator>();
        debuff = new PoisonDebuff(debufficon);
    }
    private void FixedUpdate()
    {
        if (nexfiretime < Time.time)
        { 
          nexfiretime = Time.time + CD;
          Attack();
        }
        else
       
         animator.SetBool("Attack", false);
    }


    private void Attack()
    {
         animator.SetBool("Attack", true);
         var GO  = Instantiate(explosion, transform.position, Quaternion.identity);   
    }
    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.name != "Player")
        {
            if (coll.gameObject.tag == "Fighter")
            {
                Debuff clone = debuff.Clone();

                clone.Apply(coll.gameObject.GetComponent<Fighter>());

                coll.gameObject.GetComponent<Fighter>().RecievDMG(damage, debuff);
            }
        }
    }
}

