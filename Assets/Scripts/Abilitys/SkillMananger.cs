using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillMananger : MonoBehaviour
{
    public static SkillMananger instance;

    [SerializeField] private Text name;
    [SerializeField] private Text opisanie;
    [SerializeField] private Image image;
    [HideInInspector] public Ability ability1;
    [HideInInspector] public bool IgnightTalant;

    public void ButtonPressed(Ability ability)
    {
        name.text = ability.name;
        image.sprite = ability.icon;
        opisanie.text = ability.opisanie;
        ability1 = ability;
    }

    private void Awake()
    {
        if (instance == null)
        {

            instance = this;

        }
        else
        {

            if(instance != this )
            {

                Destroy(gameObject);
            }


        }
        DontDestroyOnLoad(gameObject);
    }
}
