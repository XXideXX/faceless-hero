using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu]
public class SpellfireBall : Ability
{
    [SerializeField] private GameObject fireballpref;
    [SerializeField] private GameObject destroyFX;
    [HideInInspector]  public bool IgnighrDebuffenabeled;
    [SerializeField] private float ManaCost;

    public override void Activate(GameObject parent, Vector2 direction)
    {
        if (parent.GetComponent<Player>().ManaCapasity >= ManaCost)
        {
            parent.GetComponent<Player>().ManaCapasity -= ManaCost;
            GameObject buleetOBJ = Instantiate(fireballpref, parent.transform.position, Quaternion.Euler(0, 0, (Mathf.Atan2(direction.x, -direction.y) * 60) - 90));
            Physics2D.IgnoreCollision(parent.GetComponent<BoxCollider2D>(), buleetOBJ.GetComponent<BoxCollider2D>());
       
            buleetOBJ.GetComponent<FireBallGO>().damage = parent.GetComponent<Player>().damage;
            buleetOBJ.GetComponent<FireBallGO>().IgnightTalant = SkillMananger.instance.IgnightTalant;
            buleetOBJ.GetComponent<Rigidbody2D>().velocity = direction.normalized * 10f;
            buleetOBJ.GetComponent<FireBallGO>().debufficon = icon;
        }
    }
    public override void BeginCooldown(GameObject parent, Vector2 direction)
    {    
    }
}
