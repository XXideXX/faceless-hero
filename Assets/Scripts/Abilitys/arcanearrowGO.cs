using Assets.abilitys;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class arcanearrowGO : MonoBehaviour
{
    [HideInInspector] public int damage;
    [HideInInspector] public Sprite debufficon;
    [HideInInspector] public bool IgnightTalant;
    ArcaneDebuff debuff;
    [HideInInspector] public float heromana;
    private void Start()
    {
        
        debuff = new ArcaneDebuff(debufficon);
        debuff.MyTickDamage = ((int)heromana);
    }
    private void OnCollisionEnter2D(Collision2D coll)
    {

        if (coll.gameObject.name != "Player")
        {

            if (coll.gameObject.tag == "Fighter")
            {
              
                    Debuff clone = debuff.Clone();
                    clone.Apply(coll.gameObject.GetComponent<Fighter>());
                

                coll.gameObject.GetComponent<Fighter>().RecievDMG(damage, debuff);

            }

            Destroy(gameObject);

        }


    }
}
