using Assets.abilitys;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcaneDebuff : Debuff
{
    public int MyTickDamage { get; set; }

    private float duration = 6.05f;
    private float starttime;
    
    private float tiks = 3;
    private float tikstime;
    
    public override string Name => "Arcane";

    // {
    //       get { return; }
    // }

    public ArcaneDebuff(Sprite icon) : base(icon)   // ���� ������ ����� ������� fighter
    {
        starttime = Time.time + duration;
        tikstime = Time.time + tiks;
    }
    public override void Apply(Fighter character)
    {  
        base.Apply(character);
    }
    public override void Update()
    {
        if (tikstime < Time.time)
        {
            tikstime = Time.time + tiks;
            fighter.RecievDMG(MyTickDamage, null);
           
        }
        if (starttime < Time.time)
        {
            Remove();
        }
    }
   

    public override void Remove()
    {
      
        base.Remove();
    
    }

    public override Debuff Clone()
    {
        ArcaneDebuff clone = (ArcaneDebuff)this.MemberwiseClone();
        return clone;
    }
}
