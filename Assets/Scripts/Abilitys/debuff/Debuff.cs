﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.abilitys
{
   public abstract class Debuff
    {
        public float MyDuration { get; set; }

        public float ProcChance { get; set; }

        public Sprite MyIcon { get; set; }

        private float elpased;

        protected Fighter fighter;

        public abstract string Name
        {
            get;
        }

        public Debuff (Sprite image)
        {
            this.MyIcon = image;
        }

        public virtual void Apply(Fighter character)
        {
            this.fighter = character;
            character.ApplyDebuff(this); 
        }

        public virtual void Remove()
        {

            fighter.RemoveDebuff(this);
            elpased = 0;
        }

        public virtual void Update()
        {

            elpased += Time.deltaTime;

            if(elpased >= MyDuration)
            {
                Remove();
            }

        }

        public abstract Debuff Clone();
    }
}
