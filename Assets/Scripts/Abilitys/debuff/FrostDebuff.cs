﻿
using UnityEngine;

namespace Assets.abilitys
{
    class FrostDebuff :Debuff
    {
        public int MySpeedReduction { get; set; }

        public override string Name => "Frost";
       // {
       //       get { return; }
       // }

        private float originalspeed;

        public FrostDebuff(Sprite icon) : base(icon)   // сюда дэмэдж можно вписать fighter
        {
            MyDuration = 3; 
        }

        public override void Apply(Fighter character)
        {
            if(originalspeed != 0)
            character.skorost = originalspeed;
     
            originalspeed = character.skorost;
            character.skorost -= character.skorost - (character.skorost * MySpeedReduction/100);
            base.Apply(character);
        }

        public override void Remove()
        {
            fighter.skorost = originalspeed;
            base.Remove();
        }

        public override Debuff Clone()
        {
            FrostDebuff clone = (FrostDebuff)this.MemberwiseClone();
            return clone;
        }
    }
}

