﻿using UnityEngine;


namespace Assets.abilitys
{
    public class IgniteDebuff : Debuff
    {

        public int MyTickDamage { get; set; }

        public override string Name => "Frost";

        //    get { return "Ignite"; }

        private float elpased;

        public IgniteDebuff(Sprite icon) : base(icon)   // сюда дэмэдж можно вписать fighter
        {
            MyDuration = 20;
            MyTickDamage = 5;
          
        }

        public override void Update()
        {
            elpased += Time.deltaTime;

          
            if (elpased >= MyDuration/MyTickDamage) // хуйня от дэмэджа зависит
            {

                fighter.RecievDMG(MyTickDamage, null);
                elpased = 0;

            }

            base.Update();
        }

        public override void Remove()
        {

            elpased = 0;
            base.Remove();
        }

        public override Debuff Clone()
        {
            IgniteDebuff clone = (IgniteDebuff)this.MemberwiseClone();
            return clone;
        }
    }
}
