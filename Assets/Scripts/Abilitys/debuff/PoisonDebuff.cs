﻿
using UnityEngine;

namespace Assets.abilitys
{
    class PoisonDebuff : Debuff
    {
        public int MyTickDamage { get; set; }

        public override string Name => "Poison";
       //  get { return "Poison"; }
        

        private float elpased;

        public PoisonDebuff(Sprite icon) : base(icon)   // сюда дэмэдж можно вписать fighter
        {
            MyDuration = 10;
            MyTickDamage = 9;

        }

        public override void Update()
        {
            elpased += Time.deltaTime;


            if (elpased >= MyDuration / MyTickDamage) // хуйня от дэмэджа зависит
            {

                fighter.RecievDMG(MyTickDamage, null);
                elpased = 0;

            }

            base.Update();
        }

        public override void Remove()
        {

            elpased = 0;
            base.Remove();
        }

        public override Debuff Clone()
        {
            PoisonDebuff clone = (PoisonDebuff)this.MemberwiseClone();
            return clone;
        }
    }
}

