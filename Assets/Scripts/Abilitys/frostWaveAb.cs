using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class frostWaveAb : Ability
{
    [SerializeField] private GameObject fireballpref;
    [SerializeField] private GameObject destroyFX;
    [SerializeField] private float ManaCost;

    public override void Activate(GameObject parent, Vector2 direction)
    {
        if (parent.GetComponent<Player>().ManaCapasity >= ManaCost)
        {
            parent.GetComponent<Player>().ManaCapasity -= ManaCost;
            GameObject buleetOBJ = Instantiate(fireballpref, parent.transform.position, Quaternion.Euler(0, 0, (Mathf.Atan2(direction.x, -direction.y) * 60) - 90));
            Physics2D.IgnoreCollision(parent.GetComponent<BoxCollider2D>(), buleetOBJ.GetComponent<BoxCollider2D>());

            buleetOBJ.GetComponent<frostwaveOBJ>().damage = parent.GetComponent<Player>().damage;
     
            buleetOBJ.GetComponent<Rigidbody2D>().velocity = direction.normalized * 10f;
            buleetOBJ.GetComponent<frostwaveOBJ>().debufficon = icon;
            Destroy(buleetOBJ, 0.4f);
        }
    }
    public override void BeginCooldown(GameObject parent, Vector2 direction)
    {
    }
}
