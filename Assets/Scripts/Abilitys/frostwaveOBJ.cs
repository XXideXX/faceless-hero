using Assets.abilitys;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class frostwaveOBJ : MonoBehaviour
{

    [HideInInspector] public int damage;
    [HideInInspector] public Sprite debufficon;

    FrostDebuff debuff;
    private void Start()
    {
        
            debuff = new FrostDebuff(debufficon);
        debuff.MySpeedReduction = 50;
    }


    private void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.gameObject.name != "Player")
        {
            if (coll.gameObject.tag == "Fighter")
            {
               Debuff clone = debuff.Clone();
               clone.Apply(coll.gameObject.GetComponent<Fighter>());
               
               coll.gameObject.GetComponent<Fighter>().RecievDMG(damage, debuff);
            }
        }
    }
}

