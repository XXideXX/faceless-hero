using Assets.abilitys;
using UnityEngine;

public class poisonarrowGO : MonoBehaviour
{

    [HideInInspector] public int damage;
    [HideInInspector] public Sprite debufficon;
    
    PoisonDebuff debuff;

    private void Start()
    {
            debuff = new PoisonDebuff(debufficon);
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {

        if (coll.gameObject.name != "Player")
        {

            if (coll.gameObject.tag == "Fighter")
            {
                    Debuff clone = debuff.Clone();
                    clone.Apply(coll.gameObject.GetComponent<Fighter>());

                    coll.gameObject.GetComponent<Fighter>().RecievDMG(damage, debuff);

            }

            Destroy(gameObject);

        }


    }



}
