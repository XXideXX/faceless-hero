using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    [SerializeField] Transform celslezki ;
    [SerializeField] new Vector3 offset;
    private void Start()
    {
        celslezki= FindObjectOfType<Player>().transform;
    }
    public static Camera instanse;
    private void Awake()
    {
        if (Camera.instanse != null)
        {
            Destroy(gameObject);
            return;

        }
        instanse = this;
        DontDestroyOnLoad(gameObject);
    }
    private void FixedUpdate()
    {
        transform.position =new Vector3(celslezki.position.x + offset.x, celslezki.position.y + offset.y, -10);
    }
}
