using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colidebl : MonoBehaviour
{
    public ContactFilter2D filter;
    private BoxCollider2D Boxcollider;
    private Collider2D[] hits = new Collider2D[10];
    protected virtual void Start()
    {
        Boxcollider = GetComponent<BoxCollider2D>();
    }
    protected virtual void Update()
    {
        Boxcollider.OverlapCollider(filter, hits);
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i] == null)
                continue;
            oncollide(hits[i]);

            hits[i] = null;
        }

    }

    protected virtual void oncollide(Collider2D coll)
    {

    }
}
