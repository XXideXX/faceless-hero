
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateGameObjects : MonoBehaviour
{
    [SerializeField] [Range(0f,1f)] private float chestchance = 0.5f;

    [SerializeField] private GameObject Chest;
    [SerializeField] [Range(0f, 1f)] private float enemy_0chance = 0.5f;

    [SerializeField] private GameObject enemy_0;
 


    public void CreateChests(HashSet<Vector2Int> rooms)
    {
        foreach (var room in rooms)
        {
            if(Random.value < chestchance)
            Instantiate(Chest, new Vector2(room.x + 0.5f, room.y + 0.5f ), Quaternion.identity);
        }

    }

    public void CreateEnemy(HashSet<Vector2Int> rooms )
    {
       
        foreach (var room in rooms)
        {
            if (Random.value < enemy_0chance)
                Instantiate(enemy_0, new Vector2(room.x + 0.5f, room.y + 0.5f), Quaternion.identity);
        }
      

    }




}
