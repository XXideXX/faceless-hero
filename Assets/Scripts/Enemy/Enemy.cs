using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Mover
{
    public int xpValue = 1;
    public float triggerLenght = 1;
    private Transform playertransform;
 
    public float speed = 1;
    //public ContactFilter2D filter;
    public int damage;

    public float attackanimduration = 1;
    // public float pushforce;
    [HideInInspector] public Vector3 dir;

    private float nexfiretime;
    [SerializeField] private float lineOfSite;
    [SerializeField] private float hitingrange;

    [SerializeField] private GameObject hitbox;
    public float attacksepeedM = 1;
    Pathfinding.AIDestinationSetter pathfinding;

    private bool agroON = false;

    protected override void Start()
    {
        base.Start();
        playertransform = GameMananger.instanse.player.transform;
        var player = FindObjectOfType<Player>();
        

        Physics2D.IgnoreCollision(player.GetComponent<BoxCollider2D>(), GetComponent<BoxCollider2D>());

        // Physics2D.IgnoreLayerCollision(3, 3);
        // ������

        attackanimduration = attackanimduration / attacksepeedM ;
        pathfinding = GetComponent<Pathfinding.AIDestinationSetter>();
        skorost += UnityEngine.Random.Range(-0.4f, 0.4f);
    }

    private void FixedUpdate()
    {
        //���������� ��� ����� ������ pathfond
       
        float distancefromplayer = Vector2.Distance(playertransform.position, transform.position);
        dir = playertransform.position - transform.position;
       
        if (distancefromplayer < lineOfSite)
        {
            if(distancefromplayer < hitingrange &&  nexfiretime < Time.time)
            {
                pathfinding.target = null;
                nexfiretime = Time.time + attackanimduration;
                Attack();
            }
            else if (distancefromplayer > hitingrange *0.8)
            {    
          
               movegeroy(dir);
               pathfinding.target = playertransform;
            }
        }
        else
        {
            pathfinding.target = null;
            animator.SetBool("�����", false);
        }
           // hitbox.OverlapCollider(filter, hits); �����?
    }

    
    

    private void Attack()
    {
        animator.SetFloat("attMultiplier", attacksepeedM);
        animator.SetTrigger("�����");
        if (dir.x < 0 && transform.localScale.x > 0)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }
        else if (dir.x > 0 && transform.localScale.x < 0)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }
        var angle =  Mathf.Atan2(GetEnemyDir().x, GetEnemyDir().y) * Mathf.Rad2Deg;
       
        if (gameObject.transform.localScale.x < 0)
            angle= angle * -1 - 90;
        else
            angle = angle * -1 + 90;
        
        hitbox.transform.rotation = Quaternion.Euler(0,0,angle);
      
    }

    public Vector3  GetEnemyDir()
    {
        return dir;
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lineOfSite);
        Gizmos.DrawWireSphere(transform.position, hitingrange);
    }

    protected override void Death()
    {
        Destroy(gameObject);
        GameMananger.instanse.exp += xpValue;
        GameMananger.instanse.ShowText("+" + xpValue, transform , Color.blue);
    }
}

