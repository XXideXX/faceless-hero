using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedEnemy : Mover
{

    public int xpValue = 1;
    public float triggerLenght = 1;
    private bool collidingwhithplayer;
    private Transform playertransform;
    [SerializeField] public float firerate = 1;
    private float nexfiretime;
    private BoxCollider2D hitbox;
  
    [SerializeField] private float lineOfSite;
    public float speed = 1;
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform bulletparent;
    [SerializeField]private float shootingrange;
    [HideInInspector]  public int bulletDMG;
   // public ContactFilter2D filter;

    protected override void Start()
    {
        base.Start();
        playertransform = GameMananger.instanse.player.transform;
        var player = FindObjectOfType<Player>();
        hitbox = transform.GetChild(0).GetComponent<BoxCollider2D>();
        Physics2D.IgnoreCollision(player.GetComponent<BoxCollider2D>(), GetComponent<BoxCollider2D>());

        skorost += UnityEngine.Random.Range(-0.4f, 0.4f);
        //��������� ������� ������

    }


    private void FixedUpdate()
    {

        float distancefromplayer = Vector2.Distance(playertransform.position, transform.position);
        Vector3 dir = playertransform.position - transform.position;
      //  if (distancefromplayer < lineOfSite && distancefromplayer > shootingrange)
      //  {
         
      //  }
        if  (distancefromplayer <= shootingrange && nexfiretime  <Time.time)
        {
            animator.SetFloat("attMultiplier", 1f);
            animator.SetTrigger("�����");
            GameObject  buleetOBJ =  Instantiate(bullet, bulletparent. transform.position, Quaternion.identity);
            buleetOBJ.transform.parent = bulletparent;
            
          
            Physics2D.IgnoreCollision(buleetOBJ.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            Physics2D.IgnoreLayerCollision(9, 3);
            nexfiretime = Time.time + firerate;
           // animator.SetBool("�����", false);
        }
        movegeroy(dir);
    }
     private void OnDrawGizmosSelected()
     {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lineOfSite);
        Gizmos.DrawWireSphere(transform.position, shootingrange);
     }
    protected override void Death()
    {
        Destroy(gameObject);
        GameMananger.instanse.exp += xpValue;
        GameMananger.instanse.ShowText("+" + xpValue, transform, Color.blue);
    }
}
