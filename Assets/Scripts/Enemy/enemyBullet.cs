using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyBullet : MonoBehaviour
{
    GameObject target;
    [SerializeField] private float speed;
    Rigidbody2D bulletRB;

    [HideInInspector] public int bulletDMG;
    float pushforce = 0;
    private void Start()
    {
        bulletRB = GetComponent<Rigidbody2D>();
        target = GameObject.FindObjectOfType<Player>().gameObject;

        Vector2 moveDir = (target.transform.position - transform.position).normalized * speed;
        bulletRB.velocity = new Vector2(moveDir.x, moveDir.y);
        Destroy(this.gameObject, 2);


    }
 
    private void OnTriggerEnter2D(Collider2D collision)
    {
       
           if (collision.gameObject.name == "Player")
           {
                   collision.gameObject.GetComponent<Fighter>().RecievDMG (bulletDMG , null);
           }

    }
}


