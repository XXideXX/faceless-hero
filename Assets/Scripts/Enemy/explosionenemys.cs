using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class explosionenemys : Mover
{

    public int xpValue = 1;
    public float triggerLenght = 1;
    private Transform playertransform;

    public float speed = 1;
    //public ContactFilter2D filter;
    public int damage;
    public float attackanimduration = 1;
    // public float pushforce;
    [HideInInspector] public Vector3 dir;
    private float nexfiretime;
    [SerializeField] private float lineOfSite;
    [SerializeField] private float hitingrange;

    public float attacksepeedM = 1;
    Pathfinding.AIDestinationSetter pathfinding;
    [SerializeField] private GameObject dethFX;

    protected override void Start()
    {
        base.Start();
        playertransform = GameMananger.instanse.player.transform;
        var player = FindObjectOfType<Player>();

        Physics2D.IgnoreCollision(player.GetComponent<BoxCollider2D>(), GetComponent<BoxCollider2D>());
        Physics2D.IgnoreLayerCollision(3, 3);
        skorost += UnityEngine.Random.Range(-0.4f, 0.4f);
        attackanimduration = attackanimduration / attacksepeedM;
        pathfinding = GetComponent<Pathfinding.AIDestinationSetter>();
    }

    private void FixedUpdate()
    {
        //���������� ��� ����� ������ pathfond
        float distancefromplayer = Vector2.Distance(playertransform.position, transform.position);
        dir = playertransform.position - transform.position;

        if (distancefromplayer < lineOfSite)
        {
            if (distancefromplayer < hitingrange && nexfiretime < Time.time)
            {
                pathfinding.target = null;
                nexfiretime = Time.time + attackanimduration;
                Attack();
            }
            else if (distancefromplayer > hitingrange * 0.8)
            {
                animator.SetBool("�����", false);
                
                pathfinding.target = playertransform;
            }
        }
      
        else
        {
            pathfinding.target = null;
            animator.SetBool("�����", false);
        }
        movegeroy(dir);

    }

    private void Attack()
    {
        animator.SetFloat("attMultiplier", attacksepeedM);
        animator.SetBool("�����", true);
        if (dir.x < 0 && transform.localScale.x > 0)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }
        else if (dir.x > 0 && transform.localScale.x < 0)
        {
            transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
        }
        // StartCoroutine(Wait());
        Death();
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.4f);
        Death();
    }
    public Vector3 GetEnemyDir()
    {
        return dir;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lineOfSite);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, hitingrange);
    }

    protected override void Death()
    {
        var GameObj = Instantiate(dethFX,new Vector3 (transform.position.x , transform.position.y , 0) , Quaternion.identity);
        GameObj.GetComponent<ExplosionEnemy>().damage = damage;
        Destroy(GameObj, 0.3f);
        Destroy(gameObject);
 
    }

}
