using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionEnemy : MonoBehaviour
{
    public int damage;
    private bool hit = false;
   
    
    private void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.gameObject.name == "Player")
        {

            if (coll.gameObject.tag == "Fighter")
            {
                if(hit == false)
                {
                    
                    coll.gameObject.GetComponent<Fighter>().RecievDMG(damage, null);
                    hit = true;
                }
            }

           

        }


    }
}
