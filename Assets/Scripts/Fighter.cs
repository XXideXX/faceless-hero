using Assets.abilitys;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fighter : MonoBehaviour
{

    public int hitpoint = 10;
    public int maxHitpoints = 10;
    public float pushrecoverySpeed = 0.2f;
    public float skorost = 10f;


    protected float immunetime = 1;
    protected float lastImmune;

    private List<Debuff> debuffs = new List<Debuff>();

    private List<Debuff> newDebuffs = new List<Debuff>();

    private List<Debuff> expiredDebuffs = new List<Debuff>();

    protected Vector3 pushdirection;
    void Start()
    {

        gameObject.GetComponentInChildren<Hpbar>().SetHelth(hitpoint, maxHitpoints);
    }

    private void Update()
    {
        HandleDebuffs();
    }

    private void HandleDebuffs()
    {

        if (debuffs.Count >0)
        {
            foreach (Debuff debuff in debuffs)
            {
                debuff.Update();
            }
        }

        if(newDebuffs.Count > 0)
        {

            debuffs.AddRange(newDebuffs);
            newDebuffs.Clear();
        }

        if(expiredDebuffs.Count >0)
        {

            foreach (Debuff debuff in expiredDebuffs)
            {

                debuffs.Remove(debuff);

            }
            expiredDebuffs.Clear();
        }
    }

    public void ApplyDebuff(Debuff debuff)
    {
        Debuff tmp = debuffs.Find(x => x.Name == debuff.Name);
        if(tmp != null)
        {
            expiredDebuffs.Add(tmp);
        }
        else
        {
            gameObject.GetComponentInChildren<Hpbar>().AddDebuufIcon(debuff);
        }
        this.newDebuffs.Add(debuff);      
    }
  
    public void RemoveDebuff(Debuff debuff)
    {
        gameObject.GetComponentInChildren<Hpbar>().RemoveDebuffIcon(debuff);
        this.expiredDebuffs.Add(debuff);

    }

    public virtual void RecievDMG(int dmg , Debuff debuff)
    {
        // if (Time.time - lastImmune > immunetime)  lastImmune = Time.time; ������ ��� plyer ���� ������ �� ������� , �� �� ����� �����(��������)

        //    if (debuff != null) { ApplyDebuff(debuff); }
           
            hitpoint -= dmg;
            
          

            GameMananger.instanse.ShowText(dmg.ToString(), transform, Color.red);
            gameObject.GetComponentInChildren<Hpbar>().SetHelth(hitpoint, maxHitpoints);
            if (hitpoint <= 0)
            {
                hitpoint = 0;
                Death();
            }
       
    }

    protected virtual void Death( )
    { 
    }
}
