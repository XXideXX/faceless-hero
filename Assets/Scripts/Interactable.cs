using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Interactable : MonoBehaviour
{
    public bool isinrange;
    public UnityEvent unityEvents;
    public Button button;
  [SerializeField]  private GameObject buttonOBJ;
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.name == "Player")
        {
            isinrange = true;
            buttonOBJ.SetActive(true);
        }

      
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "Player")
        {
            isinrange = false;
            buttonOBJ.SetActive(false);
        }
    }
    private void Start()
    {
        button.onClick.AddListener(DoSomething);
    }

    public void DoSomething()
    {
        if (isinrange)
            unityEvents.Invoke();

    }
  


}
