using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;



public class InventoryCursorController : MonoBehaviour
{
    [SerializeField]
    private InventoryUIChannel InventoryUIChannel;
    [SerializeField]
    private InventoryChannel InventoryChannel;
    
    private InventorySystem.InventorySlot m_CursorSlot = new InventorySystem.InventorySlot();
    public InventorySystem.InventorySlot CursorSlot => m_CursorSlot;
    private EquippedItemsHolder equippedItemsHolder;

     private void Awake()
    {
        InventoryUIChannel.OnInventoryToggle += OnInventoryToggle;
    }

    private void Start()
    {

        equippedItemsHolder = GetComponent<EquippedItemsHolder>();
         InventorySlotUIController slotController = GetComponent<InventorySlotUIController>();
        if (slotController != null)
        {
            slotController.InventorySlot = m_CursorSlot;
        }
    }

    private void OnDestroy()
    {
        InventoryUIChannel.OnInventoryToggle -= OnInventoryToggle;
    }

    private void Update()
    {
        if (Input.touchCount > 0)//Input.GetMouseButtonDown(0)
        {
          
              InventorySystem.InventorySlot slot = FindSlotAtPosition();
         
            Touch touch = Input.GetTouch(0);
          
                if (touch.phase == TouchPhase.Ended && slot != null)    // ����� ����� ���������
                {
               
                   { 
                     try
                     {
                       m_CursorSlot.MoveAllTo(slot);
                     }
                            catch (InventorySystem.FailedToMoveItemToSlotException) { }  

                   }

               }
            else if(touch.phase == TouchPhase.Ended && slot == null && m_CursorSlot.Item != null)//��������� ������
            {

                Instantiate(m_CursorSlot.Item.CakVigliditDrop, new Vector3(0,0,0), Quaternion.AngleAxis(UnityEngine.Random.Range(-180, 180), new Vector3(0, 0, 1)));

                m_CursorSlot.Clear();
                


            }
        }


           
    }

    private InventorySystem.InventorySlot FindSlotAtPosition()
    {
        InventorySystem.InventorySlot foundSlot = null;

        PointerEventData pointerEventData = new PointerEventData(null);
        pointerEventData.position = transform.position;

        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(pointerEventData, results);

        foreach (RaycastResult result in results)
        {
            InventorySlotUIController slotController = result.gameObject.GetComponent<InventorySlotUIController>();
            if (slotController != null)
            {
                foundSlot = slotController.InventorySlot;
                break;
            }
        }

        return foundSlot;
    }

    private void OnInventoryToggle(InventoryHolder inventoryHolder)
    {
        if (m_CursorSlot.Item != null)
        {
            InventoryChannel.RaiseLootItem(m_CursorSlot.Item, m_CursorSlot.Quantity);
            m_CursorSlot.Clear();
        }
    }
}
