using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class DungeonCreator : MonoBehaviour
{

    public static DungeonCreator instanse;
 
    [SerializeField] private CompositeCollider2D grid;

    private void Awake()
    {
        if (DungeonCreator.instanse != null)
        {
            Destroy(gameObject);
            return;

        }
        instanse = this;
        DontDestroyOnLoad(gameObject);

        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    public void OnSceneLoaded(Scene scene,LoadSceneMode mode )
    {
        if (scene.buildIndex != 0)
        {
           var roomhen = GetComponentInChildren<RoomFirstDungeonGenerator>();
            roomhen.GenerateDungeaon();
            
            GenPathfainding();
           
        }

    }

    private void GenPathfainding()
    {

     
        //AstarPath.active.ScanAsync();
        AstarPath.active.UpdateGraphs(grid.bounds, 0);
        
        AstarPath.active.Scan();
    }

 

}
