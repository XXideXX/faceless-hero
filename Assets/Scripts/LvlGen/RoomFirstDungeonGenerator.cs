using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RoomFirstDungeonGenerator : SimpleRandomWalkDungeonGenerator
{
    [SerializeField] private int minroomwhid = 4, minroomhight = 4;
    [SerializeField] private int dungeonwidth = 20, dungeonhight = 20;
    [SerializeField] [Range (0,10)] private int offset = 1;
    [SerializeField] private bool randomwalkrooms = false;
    public Vector2Int firstroomcenter;
    public Vector2Int lastroomcenter;
    [SerializeField] int coridorsmultiply = 2;
    [SerializeField] private GameObject portal;
    protected override void RunProceduralGeneration()
    {


    
        createrooms();
        //????? shadowCaster2DTileMap.Generate();
    }

    private  void createrooms()
    {
        var roomList = proceduralGENalgoritms.BinariBaseParitiinoning(new BoundsInt((Vector3Int)startpos,
            new Vector3Int(dungeonwidth,dungeonhight,0)), minroomwhid , minroomhight);

       
        
        // roomList =  Chetnoecolichestvo(roomList);
        
        HashSet<Vector2Int> floor = new HashSet<Vector2Int>();
         floor = createSimplerooms(roomList);
        //  var onliroomsfloor = FindRoomsWallsDirection(floor, direction2d.DirectionList);

        List<Vector2Int> roomcenters = new List<Vector2Int>();
        foreach (var room in roomList)
        {
            roomcenters.Add((Vector2Int)Vector3Int.RoundToInt(room.center));
        }
        firstroomcenter = roomcenters[0];
        lastroomcenter = roomcenters[roomcenters.Count -1];

        HashSet<Vector2Int> roomListbez1 = new HashSet<Vector2Int>();
        roomList.RemoveAt(0);
        roomListbez1 = createSimplerooms(roomList);

        createGameObjects.CreateChests(roomListbez1);
        createGameObjects.CreateEnemy(roomListbez1);

        HashSet<Vector2Int> coridors = ConnectRooms(roomcenters);
        var coridors2x2 = Createcoridors3x3(coridors, Direction2d.cardinalDirectionList);
        teleportpiderastavmid(firstroomcenter, piderast);
        postavitportalnanewxtlvl(lastroomcenter, portal);
        coridors.UnionWith(coridors2x2);
        floor.UnionWith(coridors);

        
        tlemapVisualiser.PaintFloorTiles(floor);
       // createGameObjects.createDroos(floor, onliroomsfloor, tlemapVisualiser);
        wallGEN.CreateWalls(floor, tlemapVisualiser, shadowCaster2DTileMap);

       

    }

    private void postavitportalnanewxtlvl(Vector2Int lastroomcenter, GameObject portal)
    {
        Instantiate(portal, new Vector3(lastroomcenter.x,lastroomcenter.y,0),Quaternion.identity);
    }

    private void teleportpiderastavmid(Vector2Int firstroomcenter, Transform piderast)
    {
       piderast.transform.position = new Vector3(firstroomcenter.x, firstroomcenter.y, 0);
    }

    private static HashSet<Vector2Int> FindRoomsWallsDirection(HashSet<Vector2Int> floorposition, List<Vector2Int> directionList)
    {
        HashSet<Vector2Int> wallPOS = new HashSet<Vector2Int>();
        foreach (var position in floorposition)
        {
            foreach (var direction in directionList)
            {
                var neighbourpos = position + direction;
                if (floorposition.Contains(neighbourpos) == false)
                {
                    wallPOS.Add(neighbourpos);
                }
            }

        }
        return wallPOS;
    }
    private static List<BoundsInt> Chetnoecolichestvo(List<BoundsInt> roomList)
    {
        if (roomList.Count % 2 != 0)
        {
            roomList.RemoveAt(roomList.Count - 1);
            return roomList;
        }
        else
        return roomList;
    }

    private static HashSet<Vector2Int> Createcoridors3x3(HashSet<Vector2Int> coridors, List<Vector2Int> directionList)
    {
        HashSet<Vector2Int> coridors3x3 = new HashSet<Vector2Int>();
        foreach (var position in coridors)
        {
            foreach (var direction in directionList)
            {
                var neighbourpos = position + direction;
                if (coridors.Contains(neighbourpos) == false)
                {

                    coridors3x3.Add(neighbourpos);

                }
            }

        }
        return coridors3x3;
    }
    private HashSet<Vector2Int> ConnectRooms(List<Vector2Int> roomCenters)
    {
        HashSet<Vector2Int> corridors = new HashSet<Vector2Int>();
        var currentRoomCenter = roomCenters[Random.Range(0, roomCenters.Count)];
        roomCenters.Remove(currentRoomCenter);

        while (roomCenters.Count > 0)
        {
            Vector2Int closest = FindClosestPointTo(currentRoomCenter, roomCenters);
            roomCenters.Remove(closest);
            HashSet<Vector2Int> newCorridor = CreateCorridor(currentRoomCenter, closest);
            
            currentRoomCenter = closest;
            corridors.UnionWith(newCorridor);
          
        }
        return corridors;
    }

    private HashSet<Vector2Int> CreateCorridor(Vector2Int currentRoomCenter, Vector2Int destination)
    {
        HashSet<Vector2Int> corridor = new HashSet<Vector2Int>();
        var position = currentRoomCenter;
        corridor.Add(position);
        while (position.y != destination.y)
        {
            if (destination.y > position.y)
            {
                position += Vector2Int.up;
            }
            else if (destination.y < position.y)
            {
                position += Vector2Int.down;
            }
            corridor.Add(position);
        }
        while (position.x != destination.x)
        {
            if (destination.x > position.x)
            {
                position += Vector2Int.right;
            }
            else if (destination.x < position.x)
            {
                position += Vector2Int.left;
            }
            corridor.Add(position);
        }
        return corridor;
    }

    private Vector2Int FindClosestPointTo(Vector2Int currentRoomCenter, List<Vector2Int> roomCenters)
    {
        Vector2Int closest = Vector2Int.zero;
        float distance = float.MaxValue;
        foreach (var position in roomCenters)
        {
            float currentDistance = Vector2.Distance(position, currentRoomCenter);
            if (currentDistance < distance)
            {
                distance = currentDistance;
                closest = position;
            }
        }
        return closest;
    }


    private HashSet<Vector2Int> createSimplerooms(List<BoundsInt> roomList)
    {
        HashSet<Vector2Int> floor = new HashSet<Vector2Int>();
        foreach (var room  in roomList)
        {
            for (int col = offset; col < room.size.x - offset; col++)
            {
                for (int row = offset; row < room.size.y - offset; row++)
                {
                    Vector2Int position = (Vector2Int)room.min + new Vector2Int(col, row);
                    floor.Add(position);
                } 
                
            }
        }

        return floor;

    }
}
