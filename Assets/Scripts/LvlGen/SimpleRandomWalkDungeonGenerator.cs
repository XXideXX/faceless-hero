using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class SimpleRandomWalkDungeonGenerator : abstractdungeonGEN
{
    [SerializeField] protected SimpleRandomWalkData randomWalkData;



    protected override void RunProceduralGeneration()
    {

        HashSet<Vector2Int> floorposition = RunRandomWalk(randomWalkData, startpos);
        tlemapVisualiser.Clear();
        tlemapVisualiser.PaintFloorTiles(floorposition);
        wallGEN.CreateWalls(floorposition, tlemapVisualiser, shadowCaster2DTileMap);
    }

    protected HashSet<Vector2Int> RunRandomWalk(SimpleRandomWalkData parameters, Vector2Int position)
    {
        var curentpos = position;
        HashSet<Vector2Int> florposition = new HashSet<Vector2Int>();

        for (int i = 0; i < parameters.iteration; i++)
        {
            var path = proceduralGENalgoritms.SimpleRandomWalk(curentpos, parameters.walkLenghts);
            florposition.UnionWith(path);
            if (parameters.startRandomGEN)
            {
                curentpos = florposition.ElementAt(Random.Range(0, florposition.Count));

            }


        }
        return florposition;
    }

  
}
