using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapVisualiser : MonoBehaviour
{
   [SerializeField] private Tilemap floorTilemap , wallTileMap;


    [SerializeField] private TileBase[] flortiles;

    [SerializeField] private TileBase flortile, wallTop , wallSideRight, wallSideLeft , wallfull , wallBottom 
        , wallInnerCornerDownLeft,wallInerConnerDownRight
        , wallDiagonalConnerDownRight, wallDiagonalConnerDownLeft, wallDiagonalConnerUpRight, wallDiagonalConnerUpLeft;

    public void PaintFloorTiles(IEnumerable<Vector2Int > floorposition)
    {

        PaintFloorTiles(floorposition, floorTilemap, flortiles);

        //flortiles[UnityEngine.Random.Range(0,flortiles.Length)
    }

    internal void PaintSingleWall(Vector2Int position,string binaryType)
    {
        int typeAsInt = Convert.ToInt32(binaryType, 2);
        TileBase tile = null;
        if (WallTypesHelper.wallTop.Contains(typeAsInt))
        {
            tile = wallTop;

        }
        else if (WallTypesHelper.wallSideRight.Contains(typeAsInt)) 
        {

            tile = wallSideRight;

        }
        else if (WallTypesHelper.wallSideLeft.Contains(typeAsInt))
        {

            tile = wallSideLeft;

        }
        else if (WallTypesHelper.wallBottm.Contains(typeAsInt))
        {

            tile = wallBottom;

        }
        else if (WallTypesHelper.wallFull.Contains(typeAsInt))
        {

            tile = wallfull;

        }

            if (tile != null)
        PaintSimgleTile(wallTileMap, tile, position);

    }
    private void PaintFloorTiles(IEnumerable<Vector2Int> positions, Tilemap tilemap, TileBase[] tile)
    {
        foreach (var position in positions)
        {
            PaintSimgleTile(tilemap, tile[UnityEngine.Random.Range(0, tile.Length)], position);
        }
    }

    private void PaintTiles(IEnumerable<Vector2Int> positions, Tilemap tilemap, TileBase tile)
    {
        foreach (var position in positions)
        {
            PaintSimgleTile(tilemap, tile, position);
        }
    }

    private void PaintSimgleTile(Tilemap tilemap, TileBase tile, Vector2Int position)
    {
        var tilePOS = tilemap.WorldToCell((Vector3Int)position);
        tilemap.SetTile(tilePOS, tile);
    }
    public void Clear()
    {
        floorTilemap.ClearAllTiles();
        wallTileMap.ClearAllTiles();
    }

    internal void PaintSinglConereWall(Vector2Int position, string binarytype)
    {

        int typeAsInt = Convert.ToInt32(binarytype, 2);
        TileBase tile = null;

        if (WallTypesHelper.wallInnerCornerDownLeft.Contains(typeAsInt))
        {

            tile = wallInnerCornerDownLeft;

        }
        else if (WallTypesHelper.wallInnerCornerDownRight.Contains(typeAsInt))
        {

            tile = wallInerConnerDownRight;

        }
        else if (WallTypesHelper.wallDiagonalCornerDownLeft.Contains(typeAsInt))
        {

            tile = wallDiagonalConnerDownLeft;

        }
        else if (WallTypesHelper.wallDiagonalCornerDownRight.Contains(typeAsInt))
        {

            tile = wallDiagonalConnerDownRight;

        }
        else if (WallTypesHelper.wallDiagonalCornerUpRight.Contains(typeAsInt))
        {

            tile = wallDiagonalConnerUpRight;

        }
        else if (WallTypesHelper.wallDiagonalCornerUpLeft.Contains(typeAsInt))
        {

            tile = wallDiagonalConnerUpLeft;

        }
        else if (WallTypesHelper.wallFullEightDirections.Contains(typeAsInt))
        {

            tile = wallfull;

        }
        else if (WallTypesHelper.wallBottmEightDirections.Contains(typeAsInt))
        {

            tile = wallBottom;

        }

        if (tile!= null)
        {
            PaintSimgleTile(wallTileMap, tile, position);

        }



    }
}
