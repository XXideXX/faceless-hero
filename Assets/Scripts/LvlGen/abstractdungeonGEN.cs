using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class abstractdungeonGEN : MonoBehaviour
{
    [SerializeField] protected TilemapVisualiser tlemapVisualiser = null;
    [SerializeField] protected Vector2Int startpos = Vector2Int.zero;
    [SerializeField] protected CreateGameObjects createGameObjects = null;
    [SerializeField] protected Transform piderast;
    [SerializeField] protected ShadowCaster2DTileMap shadowCaster2DTileMap;
    public void GenerateDungeaon()
    {
        tlemapVisualiser.Clear();
        RunProceduralGeneration();

    }

    protected abstract void RunProceduralGeneration();
}
