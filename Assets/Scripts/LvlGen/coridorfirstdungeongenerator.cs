using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class coridorfirstdungeongenerator : SimpleRandomWalkDungeonGenerator
{

    [SerializeField] private int coridorLenghts = 14, coridorecount = 5;
    [SerializeField] [Range(0.1f,1f)]public float roomPercent = 0.8f;
  


    protected override void RunProceduralGeneration()
    {
        coridorfirstgenerator();
    }

    private void coridorfirstgenerator()
    {

        HashSet<Vector2Int> floorpos = new HashSet<Vector2Int>();
        HashSet<Vector2Int> potencialroompos = new HashSet<Vector2Int>();

        CreateCoridors(floorpos, potencialroompos);

        HashSet<Vector2Int> roomPOS = CreateRooms(potencialroompos);

        List<Vector2Int> deadends = FindDeadEnds(floorpos);

        CreateroomsinDeadEnd(deadends,roomPOS);

        floorpos.UnionWith(roomPOS);

        tlemapVisualiser.PaintFloorTiles(floorpos);
        wallGEN.CreateWalls(floorpos , tlemapVisualiser , shadowCaster2DTileMap);
        
    }

    private void CreateroomsinDeadEnd(List<Vector2Int> deadends, HashSet<Vector2Int> roomFlors)
    {
      
        foreach(var position in deadends)
        {
            if(roomFlors.Contains(position)==false)
            {
                var room = RunRandomWalk(randomWalkData, position);
                room.UnionWith(room);
            }

        }

    }

    private List<Vector2Int> FindDeadEnds(HashSet<Vector2Int> floorpos)
    {
        List<Vector2Int> deadends = new List<Vector2Int>();
        foreach (var position in floorpos)
        {

            int neighbpurCount = 0;
            foreach (var direction in Direction2d.cardinalDirectionList)
            {

                if(floorpos.Contains(position+ direction))
                {

                    neighbpurCount++;
                    
                }

            }

            if(neighbpurCount == 1)
            {
                deadends.Add(position);

            }
        }
        return deadends;
    }

    private HashSet<Vector2Int> CreateRooms(HashSet<Vector2Int> potencialroompos)
    {
        HashSet<Vector2Int> roomPOS = new HashSet<Vector2Int>();
        int roomtocreatecount = Mathf.RoundToInt (potencialroompos.Count * roomPercent);

        List<Vector2Int> roomstocreate = potencialroompos.OrderBy(x => Guid.NewGuid()).Take(roomtocreatecount).ToList();
        foreach (var roomposition in roomstocreate)
        {
            var roomFloor = RunRandomWalk(randomWalkData, roomposition);
            roomPOS.UnionWith(roomFloor);
        }
        return roomPOS;
    }

    private void CreateCoridors(HashSet<Vector2Int> floorpos, HashSet<Vector2Int> potencialroompos)
    {
        var curentpos = startpos;
        potencialroompos.Add(curentpos);
        for (int i = 0; i < coridorecount; i++)
        {
            var coridor = proceduralGENalgoritms.RandomWalkCoridor(curentpos, coridorLenghts);
            curentpos = coridor[ coridor.Count - 1];
            potencialroompos.Add(curentpos);
            floorpos.UnionWith(coridor);
        }


    }




}
    




