using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class proceduralGENalgoritms
{
    public static HashSet<Vector2Int> SimpleRandomWalk(Vector2Int startpos, int walkLength)
    {

        HashSet<Vector2Int> path = new HashSet<Vector2Int>();
        path.Add(startpos);
        var previospos = startpos;

        for (int i = 0; i < walkLength; i++)
        {
            var nrewpos = previospos + Direction2d.GetDirection();
            path.Add(nrewpos);
            previospos = nrewpos;
        }
        return path;
    }
    public static List<Vector2Int> RandomWalkCoridor(Vector2Int startpos, int coridorlenghth)
    {

        List<Vector2Int> coridor = new List<Vector2Int>();

        var direction = Direction2d.GetDirection();
        var curentPOS = startpos;
        coridor.Add(curentPOS);

        for (int i = 0; i < coridorlenghth ; i++)
        {
            curentPOS += direction;
            coridor.Add(curentPOS);
        }
        return coridor;
    }

    public static List<BoundsInt> BinariBaseParitiinoning(BoundsInt spacetosplit, int minWidth , int minhight)
    {

        Queue<BoundsInt> roomqueue = new Queue<BoundsInt>();
        List<BoundsInt> roomlist = new List<BoundsInt>();
        roomqueue.Enqueue(spacetosplit);
        while (roomqueue.Count >0)
        {
            var room = roomqueue.Dequeue();
            if(room.size.y >= minhight && room.size.x >= minhight  )
            {

                if(Random.value<0.5f)
                {

                    if(room.size.y>= minhight *2)
                    {
                        splitHorisontaly( minhight, roomqueue, room);

                    }
                    else if(room.size.x >= minWidth*2)
                    {
                        splitVertically(minWidth, roomqueue, room);

                    }
                    else
                    {

                        roomlist.Add(room);

                    }

                }
                else
                {
                     if (room.size.x >= minWidth * 2)
                    {
                        splitVertically(minWidth, roomqueue,  room);

                    }

                    else if (room.size.y >= minhight * 2)
                    {
                        splitHorisontaly( minhight, roomqueue ,room);

                    }
                    else
                    {

                        roomlist.Add(room);

                    }

                }

            }


        }

        return roomlist;

    }

    private static void splitVertically(int minWidth,Queue<BoundsInt> roomqueue,  BoundsInt room)
    {
        var xsplit = Random.Range(1, room.size.x);
        BoundsInt room1 = new BoundsInt(room.min, new Vector3Int(xsplit, room.size.y, room.size.z));
        BoundsInt room2 = new BoundsInt(new Vector3Int(room.min.x + xsplit, room.min.y, room.min.y), new Vector3Int (room.size.x - xsplit,room.size.y, room.size.z));
        roomqueue.Enqueue(room1);
        roomqueue.Enqueue(room2);
    }

    private static void splitHorisontaly( int minhight, Queue<BoundsInt> roomqueue, BoundsInt room)
    {
        var ysplit = Random.Range(1, room.size.y);
        BoundsInt room1 = new BoundsInt(room.min, new Vector3Int(room.size.x, ysplit, room.size.z));
        BoundsInt room2 = new BoundsInt(new Vector3Int(room.min.x, room.min.y + ysplit, room.min.z),
            new Vector3Int(room.size.x, room.size.y - ysplit, room.size.z));
        roomqueue.Enqueue(room1);
        roomqueue.Enqueue(room2);
    }
}



public static class Direction2d
{
    public static List<Vector2Int> cardinalDirectionList = new List<Vector2Int>
    {
        new Vector2Int(0,1),
        new Vector2Int(1,0),
        new Vector2Int(0,-1),
        new Vector2Int(-1,0),
    };
    public static List<Vector2Int> diagonalDirectionList = new List<Vector2Int>
    {
        new Vector2Int(1,1),
        new Vector2Int(1,-1),
        new Vector2Int(-1,-1),
        new Vector2Int(-1,1),
    };

    public static List<Vector2Int> eightdirectionList = new List<Vector2Int>
    {

        new Vector2Int(0,1),
        new Vector2Int(1,1),
        new Vector2Int(1,0),
        new Vector2Int(1,-1),
        new Vector2Int(0,-1),
        new Vector2Int(-1,-1),
        new Vector2Int(-1,0),
        new Vector2Int(-1,1),

    };


    public static Vector2Int GetDirection()
    {
        return cardinalDirectionList[Random.Range(0, cardinalDirectionList.Count)];
    }
}