using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class wallGEN
{

    /*    public static void createDroosA(HashSet<Vector2Int> floorposition, HashSet<Vector2Int> roomsfloor, TilemapVisualiser tilemapVisualiser)
        {
            var basicDoorsPosition = FindWallsDirection(roomsfloor, Direction2d.cardinalDirectionList);
            var basicWallPosition = FindWallsDirection(floorposition, Direction2d.cardinalDirectionList);

            basicDoorsPosition.ExceptWith(basicWallPosition);

            foreach (var position in basicDoorsPosition)
            {
                Debug.Log(111);
                tilemapVisualiser.PaintSingleWall(position);
            }

        }*/


    public static void CreateWalls(HashSet<Vector2Int> floorposition,TilemapVisualiser tilemapVisualiser,ShadowCaster2DTileMap shadowCaster2DTileMap)
    {

        var basicWallPosition = FindWallsDirection(floorposition, Direction2d.cardinalDirectionList);
        var cornerWallPosition = FindWallsDirection(floorposition, Direction2d.diagonalDirectionList);
        CreateBasicWall(tilemapVisualiser, basicWallPosition, floorposition);
        CreateCornerWals(tilemapVisualiser, cornerWallPosition, floorposition);
        //shadowCaster2DTileMap.Generate();
    }

    private static void CreateCornerWals(TilemapVisualiser tilemapVisualiser, HashSet<Vector2Int> cornerWallPosition, HashSet<Vector2Int> floorposition)
    {
        foreach (var position in cornerWallPosition)
        {
            string neighborBinariTipe = "";
            foreach (var dierction in Direction2d.eightdirectionList)
            {
                var neighbourPOS = position + dierction;
                if(floorposition.Contains(neighbourPOS))
                {

                    neighborBinariTipe += "1";

                }
                else
                {

                    neighborBinariTipe += "0";

                }

            }

            tilemapVisualiser.PaintSinglConereWall(position, neighborBinariTipe);
        }
    }

    private static void CreateBasicWall(TilemapVisualiser tilemapVisualiser, HashSet<Vector2Int> basicWallPosition, HashSet<Vector2Int> floorposition)
    {
        foreach (var position in basicWallPosition)
        {
            string neighboursBinaryType = "";
            foreach (var direction in Direction2d.cardinalDirectionList)
            {
                var neighbourPOS = position + direction;
                if(floorposition.Contains(neighbourPOS))
                {
                    neighboursBinaryType += "1";

                }
                else
                {
                    neighboursBinaryType += "0";

                }
            }
            tilemapVisualiser.PaintSingleWall(position,neighboursBinaryType);
        }
    }

    private static HashSet<Vector2Int> FindWallsDirection(HashSet<Vector2Int> floorposition, List<Vector2Int> directionList)
    {
        HashSet<Vector2Int> wallPOS = new HashSet<Vector2Int>();
        foreach (var position in floorposition)
        {
            foreach (var direction in directionList)
            {
                var neighbourpos = position + direction;
                if( floorposition.Contains(neighbourpos)== false)
                {
                    wallPOS.Add(neighbourpos);
                }
            }

        }
        return wallPOS;
    }
}
