using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Mover
{
    [SerializeField] FloatingJoystick movenetJoystick;

    [SerializeField] private Vector2 offset;

    bool weaponequipt;
    GameObject weponpefab;

    [HideInInspector] public  Vector2 hitdirection;
    public int damage;
    public float hitrate = 1;

  


    [SerializeField] private float lineOfSite;

    private float RegeManantime = 2;
    private float lastRegenManaTime;
    public float MaxManaCapasity;
    public float ManaCapasity;
    public float ManaregenPOCENT;
    public static Player instanse;
    private void Awake()
    {
        if (Player.instanse != null)
        {
            Destroy(gameObject);
            return;

        }
        instanse = this;
        DontDestroyOnLoad(gameObject);
    }
    protected override void Start()
    {
        base.Start();
    }

 
   
    void FixedUpdate()
    {
         //hitdirection = (Vector3.up * hitsJoystick.Vertical + Vector3.right * hitsJoystick.Horizontal);      
        //movement = new Vector2(nput.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        movement = (Vector3.up * movenetJoystick.Vertical + Vector3.right * movenetJoystick.Horizontal);
        
        movegeroy(movement);
        if (ManaCapasity < MaxManaCapasity && lastRegenManaTime < Time.time) // �� ���� ��������� ������ ���� ���� �������
        {
            lastRegenManaTime = Time.time + RegeManantime;
            ManaCapasity += MaxManaCapasity * ManaregenPOCENT / 100;
            if (ManaCapasity > MaxManaCapasity)
                ManaCapasity = MaxManaCapasity;
        }
        /*
            rotatez = Mathf.Atan2(hitsJoystick.Vertical, hitsJoystick.Horizontal);
            // gameObject.transform.rotation = Quaternion.Euler(0, 0, rotatez);

           if(hitdirection.x != 0 && hitdirection.y != 0)      
            {
                animator.SetBool("SwordAttaking", true);
            }
           else
            {
                animator.SetBool("SwordAttaking", false);
            }
        */

        //GameMananger.instanse.ShowText(dmg.ToString(), transform, Color.red);
    }
    
   public void CreateItemsInWheponslot( InventorySystem.InventorySlot inventorySlot)
    {

        if (inventorySlot.Item != null)
        {
            weaponequipt = true;
          
        }
        if (inventorySlot.Item == null)
        {

            weaponequipt = false;
            Destroy(weponpefab);
        }

       
         if (weaponequipt)
        {
            if (weponpefab != null)
            { Destroy(weponpefab); }
            if (gameObject.transform.localScale.x < 0)
            { weponpefab = Instantiate(inventorySlot.Item.Itimid, transform.position + new Vector3(offset.x, offset.y, 0), Quaternion.identity); }
            else
             {
                weponpefab = Instantiate(inventorySlot.Item.Itimid, transform.position + new Vector3(-offset.x, offset.y, 0), Quaternion.identity);
            }

            weponpefab.transform.parent = gameObject.transform;
           
        }
       
    }

    public  void DropItem(InventorySystem.InventorySlot slot)
    {
        Instantiate(slot.Item.CakVigliditDrop,  transform.position, Quaternion.AngleAxis(UnityEngine.Random.Range(-180, 180), new Vector3(0, 0, 1)));
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, lineOfSite);
      
    }
}   


    