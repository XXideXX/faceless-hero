using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonHighlighter : MonoBehaviour
{
  
    public GameObject[] highlightGOS;
    public Button[] buttons;

    [HideInInspector]public int buttonNUM;
    public void ActivateHighlight(int index)
    {
        foreach (var item in highlightGOS)
        {
            item.SetActive(false);
        }
        buttonNUM = index;
        highlightGOS[index].SetActive(true);

    }
}
