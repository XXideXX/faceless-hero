using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoseButton : MonoBehaviour
{

    ButtonHighlighter highlightButton;
    [SerializeField] private Sprite emptyslot;
    [SerializeField] private AbilityButton[] abilityButtons;
    Player piderast;
    [SerializeField] private Button[] spellbuttons;


    private void Start()
    {
        highlightButton = FindObjectOfType<ButtonHighlighter>();
    }

   public void SelectSpell()
    {
        if (SkillMananger.instance.ability1 != null)
        {
            foreach (var item in highlightButton.buttons)
            {
                if (item.image.sprite == SkillMananger.instance.ability1.icon)
                {
                    item.image.sprite = emptyslot;
                }
            }
            foreach (var item in abilityButtons)
            {
                if (item.ability == SkillMananger.instance.ability1)
                {
                    item.gameObject.GetComponentsInChildren<Image>()[1].sprite = emptyslot;

                    item.ability = null;

                    item.gameObject.SetActive(false);
                }
            }
            //piderast.Chose(SkillMananger.instance.ability1, highlightButton.buttonNUM);
            spellbuttons[highlightButton.buttonNUM].gameObject.GetComponentsInChildren<Image>()[1].sprite = SkillMananger.instance.ability1.icon;

            if (SkillMananger.instance.ability1.active)
            {
                spellbuttons[highlightButton.buttonNUM].gameObject.SetActive(true);
            }
            else
            { 
                spellbuttons[highlightButton.buttonNUM].gameObject.SetActive(false);
            }


            spellbuttons[highlightButton.buttonNUM].gameObject.GetComponent<AbilityButton>().ability = SkillMananger.instance.ability1;
            IgnightTalantCheck();
            
            highlightButton.buttons[highlightButton.buttonNUM].image.sprite = SkillMananger.instance.ability1.icon;
        }  
    }

    private void IgnightTalantCheck()
    {
        foreach (var item in abilityButtons)
        {
            if (item.ability != null)
            {
                if (item.ability.name == "IgnightTalant")
                {
                    SkillMananger.instance.IgnightTalant = true;

                    break;
                }
                else
                    SkillMananger.instance.IgnightTalant = false;
            }
            SkillMananger.instance.IgnightTalant = false;
        }
    }
}
