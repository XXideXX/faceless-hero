using Assets.abilitys;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hpbar : MonoBehaviour
{
    [SerializeField] private Slider slider;
    [SerializeField] private Color Low;
    [SerializeField] private Color High;
    [SerializeField] private Vector3 offset;
    [SerializeField] private Vector3 debuff;
    [SerializeField] private GameObject debuffslot;
    [SerializeField] private GameObject debuffSlotsParrent;
    private List<GameObject> debufsicons = new List<GameObject>();
    // Start is called before the first frame update
    

    public void SetHelth (float hitpoint , float maxHitpoints)
    {
    
        slider.gameObject.SetActive(hitpoint < maxHitpoints);
        // slider.maxValue = maxHitpoints;
        slider. value = (hitpoint/ maxHitpoints) ;
        slider.fillRect.GetComponentInChildren<Image>().color = Color.Lerp(Low, High, slider.normalizedValue);
    }
    public void AddDebuufIcon(Debuff debuff)
    {
      var childObj =  Instantiate(debuffslot, debuffSlotsParrent.transform);
        debufsicons.Add(childObj);
      childObj.GetComponent<Image>().sprite = debuff.MyIcon;
    
    }
    public void RemoveDebuffIcon(Debuff debuff)
    {
        foreach (var item in debufsicons)
        {
            if(item.GetComponent<Image>().sprite == debuff.MyIcon)
            {
                debufsicons.Remove(item);
                Destroy(item);
               
            }
            return;
        }
        
    }
    
    void Update()
    {
        slider.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(transform.parent.position + offset);
        debuffSlotsParrent.transform.position = UnityEngine.Camera.main.WorldToScreenPoint(transform.parent.position + debuff);
    }
}
