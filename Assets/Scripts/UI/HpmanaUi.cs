using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpmanaGigantskayahuynya : MonoBehaviour
{
    [SerializeField] private Slider hp;
    [SerializeField] private Slider mana;
    Player piderast;
    private float lastupdatatetime;
    private float updateCD = 0.2f;
    private void Start()
    {
        piderast = FindObjectOfType<Player>();
    }
    private void Update()//����� ����������
    {
        if (lastupdatatetime < Time.time)
            lastupdatatetime += updateCD;
        hp.value = (piderast.hitpoint / piderast. maxHitpoints);
        mana.value = (piderast.ManaCapasity / piderast.MaxManaCapasity);
    }
}
