using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollVievController : MonoBehaviour
{

    [SerializeField] private InventraNeUdalyaemiy neUdalyaemiy;
    [SerializeField] Transform buttonparrent;
    [SerializeField] GameObject seedButton;
    [SerializeField] GameObject spraytsnazvaniem;
    [SerializeField] Text text;

    public void LoadInventoryButtons()// ������ ����� ����� ���� �������� ���������
    {
        foreach (   ScriptNerasrusaemgoobecta item in neUdalyaemiy.nerazrusiemismotci)
        {

            GameObject lvlbuttonsOBJ = Instantiate(seedButton, buttonparrent) as GameObject;
            lvlbuttonsOBJ.GetComponent<SeedButton>().image.sprite = item.sprite;
            lvlbuttonsOBJ.GetComponent<SeedButton>().storedData = item;
        }



    }

    public void DeleteItems()
    {
        foreach(Transform child   in buttonparrent)
        {
            GameObject.Destroy(child.gameObject);
                
        }
        
    }

    public void ShovChosenItemData(Sprite sprite , string txt , ScriptNerasrusaemgoobecta script)
    {
       
        spraytsnazvaniem.GetComponent<Button>().image.sprite = sprite;
        spraytsnazvaniem.GetComponent<slecteeditembutton>().storedDataOBJ = script;
        text.text = txt;


    }


}
