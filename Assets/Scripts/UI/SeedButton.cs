using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeedButton : MonoBehaviour
{
 
    [HideInInspector] public ScrollVievController vievController;
   [HideInInspector] public Image image;
    public ScriptNerasrusaemgoobecta storedData;

    private void Start()
    {

        vievController = FindObjectOfType<ScrollVievController>();
    }

    public void OnButtonClick()
    {

        vievController.ShovChosenItemData(storedData.sprite, storedData.name, storedData);

    }
}
