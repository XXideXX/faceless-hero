using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SimpleRandomwalkParametrs_", menuName = "RCG/SimpleRandomwalkDATA")]

public class SimpleRandomWalkData : ScriptableObject
{

    public int iteration = 10, walkLenghts = 10;
    public bool startRandomGEN = true;


}
