﻿using UnityEngine;
using System.Collections;

public class FloatingTextController : MonoBehaviour {
   [SerializeField] private  FloatingText popupText;
   [SerializeField] private  GameObject canvas;

   

    public  void CreateFloatingText(string text, Transform location , Color color)
    {
        FloatingText instance = Instantiate(popupText);
        Vector2 screenPosition = UnityEngine.Camera.main.WorldToScreenPoint(new Vector2(location.position.x + Random.Range(-.2f, .2f), location.position.y + Random.Range(-.2f, .2f)));

        instance.transform.SetParent(canvas.transform, false);
        instance.transform.position = screenPosition;
        instance.SetText(text , color);
    }
}
